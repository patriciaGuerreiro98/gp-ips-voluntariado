export interface ProjetoResponse {
  success: boolean,
  projetoId: string,
}

export interface ImageResponse {
  success: boolean,
  msg: string
}
