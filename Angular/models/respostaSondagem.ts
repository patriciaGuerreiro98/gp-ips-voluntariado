export class RespostaSondagem{
    _id: string;
    userId: string;
    sondagemId: string;
    opcoes: [string];
    outraResposta: string
}