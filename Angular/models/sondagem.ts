export class Sondagem {
  _id: string;
  titulo: string;
  descricao: string;
  opcoes: [string];
}
