import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FotoService } from './services/foto.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'test';

}
